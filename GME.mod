version="0.8.2"
tags={
	"Balance"
	"Gameplay"
	"Fixes"
	"Expansion"
	"Graphics"
	"Historical"
}
name="Great Monuments Expanded"
picture="great_monuments_expanded.png"
supported_version="1.33.3"
remote_file_id="2469419235"